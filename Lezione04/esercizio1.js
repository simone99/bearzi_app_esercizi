console.log("\"Milano\" è di tipo "+(typeof "Milano"));
console.log("8+8/3 è di tipo "+ (typeof (8+8/3)));
console.log("[\"uno\",2,3+1] è di tipo "+ (typeof ["uno",2,3+1]));
console.log("undefined è di tipo "+ typeof undefined);
console.log("null è di tipo "+ (typeof null));
console.log("\"Uno\"+2 è di tipo "+(typeof("Uno"+2)));
console.log("NaN è di tipo "+ (typeof NaN));
console.log("\"false\" è di tipo "+ (typeof "false"));
console.log("true è di tipo "+(typeof true));