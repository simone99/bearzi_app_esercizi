var dictionary = {
    "il computer di mio padre":{
        en: "my father's notebook",
        es: "el computer de mi padre"
    },
    nomeDictionary: "traduzioni"
};

var testoDaTradurre = "il computer di mio padre";
var testoTradottoEn = dictionary[testoDaTradurre].en;
var testoTradottoEs = dictionary[testoDaTradurre].es;


//var nome = dictionary.nomeDictionary;
var nome = dictionary["nomeDictionary"];

console.log("testo da tradurre:"+testoDaTradurre);
console.log("testo da tradurre:"+testoDaTradurreEn);
console.log("testo da tradurre:"+testoDaTradurreEs);
