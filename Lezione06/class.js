class Being{
    constructor(){
        console.log("I am a being.");
    }
    sayHi(){
        console.log("Hi");
        //this._medtodoPrivato();
    }
    /*_medtodoPrivato(){
        console.log("privato");
    }*/
}

class Animal extends Being{
    constructor(age){
        super();
        this.age= age;
        this.favourite_food = "";
        this.noise = "";
        /*var _this = this;*/
        setTimeout(()=>this.makeNoise(), 1000);
        //setTimeout(()=>this.makeNoise(), 1500);
        console.log("I am a animal and I am "+this.age+" years old");
    }
    run(){
        console.log("I am running");
    }
    
    eat(){
        console.log("I eat "+this.favourite_food);
    }
    
    makeNoise(){
        console.log(this);
        console.log(this.noise);
    }
}



class Dog extends Animal{
 constructor(age){
     super(age);
     this.favourite_food = "Meat";
     this.noise = "Bau";
     this._noiseInterval = setInterval(()=>this.makeNoise(),2000);
     console.log("I am a Dog and I am "+this.age+" years old");
 }
 shhht(){
    clearInterval(this._noiseInterval);
 }
}

class Cat extends Animal{
    constructor(age){
    super(age);
    this.favourite_food = "Fish";
    this.noise = "Miao";
    console.log("I am a Cat and I am "+this.age+" years old");
 }
 run(){
     super.run();
     console.log("I am run like a Cat");
 }
}

var b = new Being();
b.sayHi();

console.log("-------------------------");

var a = new Animal(5);
console.log(a.age);

console.log("-------------------------");

var d = new Dog(2);
d.run();
d.eat();
d.makeNoise();
console.log("-------------------------");

var c = new Cat(3);
c.run();
c.eat();
c.makeNoise();