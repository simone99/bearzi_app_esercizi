class Being {
    constructor() {
        console.log("new Being");
    }
    sayHi() {
        console.log("Hi, I am a being");
    }
}

class Animal extends Being {
    constructor(age) {
        super();
        this.age = age;
        console.log("new Animal");
    }
    sayHi() {
        console.log("Hi, I am an Animal and I am "+this.age);
    }
    run() {
        console.log("I am Running");
    }
    eat() {
        console.log("I am eating");
    }
}

class Cat extends Animal {
    constructor(age) {
        super(age);
        this.favourite_food = "Fish";
        console.log("new Cat");
    }
    sayHi() {
        console.log("Hi, I am a Cat and I am "+this.age);
    }
    makeNoise() {
        console.log("Miao");
    }
    eat() {
        super.eat();
        console.log(this.favourite_food);
    }
}

class Dog extends Animal {
    constructor(age) {
        super(age);
        this.favourite_food = "Meat";
        console.log("new Dog");
    }
    sayHi() {
        console.log("Hi, I am a Dog and I am "+this.age);
    }
    makeNoise() {
        console.log("Bau");
    }
    eat() {
        super.eat();
        console.log(this.favourite_food+" and I like it very much");
    }
}

var c = new Cat(3);
c.sayHi();
c.run();
c.makeNoise();
c.eat();

console.log("---------");

var d = new Dog(2);
d.sayHi();
d.run();
d.makeNoise();
d.eat();