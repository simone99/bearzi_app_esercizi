//arrayClass.js final version

//utilizzo della funzione javascript pop (toglie l'ultimo valore dell'arrray)
var arr = [1,2,3,4,5,6];
console.log(arr);
//visualizzo l'elemento che viene tolto
var removed_item = arr.pop();
console.log("valore rimosso: "+removed_item);

console.log("------------------------------------");

console.log("array originale"); 
console.log(arr);

//inversione dell'array (va a sostituire l'array originale)
var reversed_array = arr.reverse();
console.log("array invertito");
console.log(arr);
console.log(reversed_array);

console.log("------------------------------------");

var shifted_var = arr.shift();
console.log("rimuove il primo elemento: "+shifted_var);/*rimuove il primo elemento in base all'array invertito 
                                                       che va a sostituire l'array originale*/
console.log(arr);

console.log("------------------------------------");

//utilizzo della funzione javascript splice
//voglio aggiungere 5 e 6 dopo il 3
arr.splice(2,0,5,6);
console.log(arr);

//voglio togliere il secondo valore
var removed_item = arr.splice(1,1);
console.log(arr);

console.log("------------------------------------");

arr.unshift(11,12);//è il contrario di shift
console.log(arr);

console.log("------------------------------------");

var joined = arr.join("-");
console.log(joined);

console.log("------------------------------------");

//come invertire una stringa
var frase = "Frase un po' lunga da invertire in javascript";
var invertita = frase.split("").reverse().join("");
console.log("frase originale: "+frase);
console.log("frase invertita: "+invertita);

console.log("------------------------------------");

//come creare una copia dell'array originale (deccomentare var arr2= arr1)
var arr1 = ["Rosso","Verde","Giallo","Nero"];
var arr2 = arr1.join("/").split("/");
//questo genera solo un puntatore, quindi è lo stesso array con due nomi diversi

//arr1.join("/").split("/");//non influisce perchè il valore originale viene perso
//var arr2= arr1; 

arr2.reverse();//inversione dell'array
console.log(arr1);
console.log(arr2);

console.log("------------------------------------");

//utilizzo della funzione javascript charAt e length (per la lunghezza delle stringhe e il ritorno di un determinato elemento) 
console.log(frase.charAt(1));
console.log(frase.charAt(frase.length-1));

console.log("------------------------------------");

console.log(frase.indexOf("javascript"));
console.log(frase.charAt(35));
console.log(frase.indexOf("Javascript"));

console.log("------------------------------------");

//utilizzo della funzione replace per la sostituzione
frase2 = frase.replace("javascript", "php"); //php va a sostituire la stringa javascript
console.log("frase originale: "+frase);
console.log("frase sostituita: "+frase2);

console.log("------------------------------------");

frase2 = frase.replace("a", "?"); //? va a sostituire la stringa a
console.log("frase originale: "+frase);
console.log("frase sostituita: "+frase2);

console.log("------------------------------------");

//sostituzione della frase tramite l'utilizzo di un ciclo/loop
var frase2 = frase;
var changed=true;

while(changed){
    
    var temp = frase2.replace("a","?");
    
    if(temp==frase2){
        changed=false;
    }else{
        frase2= temp;
    }
    console.log(frase2);
}
console.log(frase);
console.log(frase2);

