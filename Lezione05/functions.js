//functions.js

function sum(a,b, callback){
    var risultato = a+b;

    if(typeof callback == "function") callback(risultato);
    return risultato;

}
function quad(valore){
    console.log("il quadrato di "+ valore+" è "+(valore*valore));
}

function double(valore){
    console.log("il doppio di valore è "+(valore*2));
}

console.log(sum(5,6,quad));
console.log(sum(15,46,double));
console.log(sum(45,64));

var stringa = "Buongiorno a tutti!"
//console.log(stringa.split(""));
var numero = 54895574
numero = numero.toString();
/*console.log(numero.split(""));non si può utlizzare la funzione split con i numeri, 
ma solo con le stringhe*/

//console.log(stringa.split(""));

console.log("-----------------");

//conversione da stringa a numero
var num2 = "456789";
console.log(typeof num2);
console.log(typeof parseInt(num2));

console.log("-----------------");

//stringhe in maiuscolo e in minuscolo
var nome = "Mario"
var nomeMaiuscolo = nome.toUpperCase(); //toUpperCase per trasformare una stringa in maiuscolo
console.log(nomeMaiuscolo);

var nomeMinuscolo = nome.toLowerCase(); //toLowerCase per trasformare una string in minuscolo
console.log(nomeMinuscolo);
